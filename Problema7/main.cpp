#include <iostream>

using namespace std;
void minuscula(string &palabra){
    for (int i = 0;i <palabra.length();i++){
        palabra[i] = tolower(palabra[i]);
    }
}


int main()
{
    /* Este programa recibe una cadena y elimina los caracteres que se repiten, por medio de arreglos que contienen las letras
    y un contador que contendrá la cantidad de cada letra ya ingresada; el programa comprueba constantemente si la letra
    ya fue ingresada con anterioridad, imprimiendo solo los que no han sido puestos antes.*/

    char letras[26]={'a','b','c','d','e','f','g','h','i', 'j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
    int contadorLetrasDisponibles[26]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; //Contador que contiene la cantidad de letras ingresadas

    char letraPalabra,letraArreglo;
    string palabraNormal;
    cout <<"Ingrese una palabra: "<<endl;
    cin >> palabraNormal;
    minuscula(palabraNormal);

    cout << "Original: "<<palabraNormal<< " Sin repetidos: ";

    int limite = palabraNormal.length();    //Limite para el ciclo siguiente, que recorre la palabra

    for (int i=0;i < limite;i++){
        letraPalabra=palabraNormal[i];      //Selecciona la Letra en la posicion i
        for (int c =0; c < 26;c++){         //Ciclo que recorre las letras del arreglo en busca de la misma a la anterior
            letraArreglo=letras[c];
            if (letraArreglo == letraPalabra){
                if (contadorLetrasDisponibles[c]<1){    // Si no se ha usado la letra antes, se le da a entender al contador
                    contadorLetrasDisponibles[c]+=1;    // que ya se puse una
                    cout << letraPalabra;               // e imprime la letra no puesta anteriormente
                    break;
                }
            }

        }
    }
    cout << "\n";
    return 0;
}
